class PetersonTuner:
    def __init__(self, name=None):
        match name:
            case 'ClipHDC':
                self.idProduct = 0x8007  # TODO
            case _:
                raise NotImplementedError

    def startCLI(self, *args, **kwargs):
        pass


if __name__ == '__main__':
    p = PetersonTuner(name='ClipHDC')
    p.startCLI(*args, **kwargs)
