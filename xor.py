FILENAME = 'input.sfu'

import array


with open(FILENAME, 'rb') as f:
    contents = f.read()

a = array.array('I', contents)

last_xors = [0] * 20
xor = 0
for i, word in enumerate(reversed(a)):
    xor ^= word
    idx = len(a) - i - 1
    if idx < 20:
        last_xors[idx] = xor

print([hex(elem) for elem in last_xors])
