StroboLinux
===========

_Peterson StroboClip Client for Linux_

## Discovery

On plugging in the device, the screen immediately turns on, a charge symbol lights inside the battery indicator, and a new `/dev/hidraw<n>` device appears. Typing `lsusb`:

```text
Bus 003 Device 006: ID 2368:0009 Peterson Electro-Musical Products Inc. StroboClip HDC
```

<details>
<summary>lsusb -vvv</summary>
<pre>
Device Descriptor:
  bLength                18
  bDescriptorType         1
  bcdUSB               2.00
  bDeviceClass            0 [unknown]
  bDeviceSubClass         0 [unknown]
  bDeviceProtocol         0
  bMaxPacketSize0        64
  idVendor           0x2368 Peterson Electro-Musical Products Inc.
  idProduct          0x0009 StroboClip HDC
  bcdDevice            2.00
  iManufacturer           1 Peterson EMP Inc
  iProduct                2 StroboClip HDC
  iSerial                 3 0747##323####334####003#
  bNumConfigurations      1
  Configuration Descriptor:
    bLength                 9
    bDescriptorType         2
    wTotalLength       0x0029
    bNumInterfaces          1
    bConfigurationValue     1
    iConfiguration          0
    bmAttributes         0xc0
      Self Powered
    MaxPower              200mA
    Interface Descriptor:
      bLength                 9
      bDescriptorType         4
      bInterfaceNumber        0
      bAlternateSetting       0
      bNumEndpoints           2
      bInterfaceClass         3 Human Interface Device
      bInterfaceSubClass      0 [unknown]
      bInterfaceProtocol      0
      iInterface              0
        HID Device Descriptor:
          bLength                 9
          bDescriptorType        33
          bcdHID               1.10
          bCountryCode            0 Not supported
          bNumDescriptors         1
          bDescriptorType        34 Report
          wDescriptorLength      28
          Report Descriptors:
            ** UNAVAILABLE **
      Endpoint Descriptor:
        bLength                 7
        bDescriptorType         5
        bEndpointAddress     0x81  EP 1 IN
        bmAttributes            3
          Transfer Type            Interrupt
          Synch Type               None
          Usage Type               Data
        wMaxPacketSize     0x0040  1x 64 bytes
        bInterval               1
      Endpoint Descriptor:
        bLength                 7
        bDescriptorType         5
        bEndpointAddress     0x01  EP 1 OUT
        bmAttributes            3
          Transfer Type            Interrupt
          Synch Type               None
          Usage Type               Data
        wMaxPacketSize     0x0040  1x 64 bytes
        bInterval               1
</pre>
</details>

It honestly looks like `iSerial` is 0x7 followed by a short ASCII string G###### followed by a single uppercase letter followed by a single number. The ASCII string numbers could also be their own ASCII string as they are numbers between 30 and 50. Who knows?

There is a **LCD test mode** as well as a **demo** hidden in the settings menu. Go to either firmware version (for test mode) or hardware version (for demo) and then hold the left and right buttons simultaneously. Be forewarned, if you hold these buttons again in test mode, "NEW" will appear and the device will immediately factory reset after you release the buttons.

### Teardown

Hey! I risked my valuable electronics so you don&rsquo;t have to!

Using a smartphone screen removal tool (various plastic wedge things) I pried carefully near the USB-C port and then around each edge until the plastic snap joints flexed apart and the case halves could be removed.

The PCB greets you with a handful of transistors and/or voltage regulators (five 3-pin things), a few Schottky diodes, six plated test points in a row with contact points along the edge; let us call those&hellip; I dunno&hellip; power, ground, reset, clock, data in, data out (or optionally bi-directional data and serial pin), in other words **a debug port** but that probably is a formality because the main chip is an **STM32L073RBT6**. That is a very, very standard Cortex-M0+ with not many options for encryption/obfuscation&mdash;particularly when the firmware upgrades are sent via web browser.

Rounding out the collection are a 4057A IC for charging the internal battery, UL26 ESD suppressor, probably an RGB controller (part number is strange: GGB R02 or similar) as well as "R", "G", "B", and "+" LEDs; a clearly marked but manufacturer-elusive 8.000 MHz crystal, and a rail-to-rail op amp (SGM8536).

What makes me the most happy about this entire layout is the real likelihood that something like the RGB color would be saved at a place in memory where you would be able to patch it easily if the flash controller is accessible via USB and can erase single sectors. You would still have ten colors, but perhaps the ten colors you love. Maybe all just shades of the same general color? Different brightness levels?

This would be the same for the button hold time before the tuner turns on or off.

These should be really easy to find with a raw, unencrypted firmware file. Load the SVD for that microcontroller into a disassembler, look for memory references to GPIO input (most likely only used by the buttons), see when a timer value is assigned, and that does it. It becomes even easier if one of the low-power sleep modes is involved for turning on the tuner.

I didn't look at the microphone or LCD side of the board because these did not feel nearly as important as knowing the microcontroller core. Having a loose idea of pin connections is an added bonus.

<mark>TODO: add pictures</mark>

### Peterson Connect

```json
    {
        name: "StroboClip HDC Update Mode",
        modelID: 9,
        memory: 14336,
        fill: 255,
        mode: 0,
        creds: {
            vendorId: 9064,
            productId: 32777
        }
    },
    {
        name: "StroboClip HDC",
        modelID: 9,
        memory: 14336,
        fill: 255,
        mode: 1,
        creds: {
            vendorId: 9064,
            productId: 9
    }
```

Well, that is official. This has an update mode for the udev rule list.

By the way, hex(14336) = 0x3800

A bit of _Inspect_ and what do we find? [firmware](https://www.petersontuners.com/firmware/StroboClipHDC-1_0_15.sfu).

Unfortunately, this is where we discover the beginning of this file is not .ELF or a reset vector. No, no&hellip; The first line is `SONUUSFW&;Q.h#..StroboClip HDC` and then some zeroes and then gibberish.

Looking around, there is a SONUUS that makes a firmware updater utility. They create audio gear and seem to have some Peterson-branded products. If this was meant to be a simple drop-in firmware, it appears now that there will be extra steps.

First stop: **binwalk**

According to binwalk, there is high entropy (97%) until address 0xDC00, where it drops to 83%. Actually, at this point every 128 bytes repeats:

```text
0000dc00: 89cbbda1482f0aeb 63280a3f02e7cc5b 506fca3a0c5feaa9 e3d4465ebed57960 a1cdc9e935685929 566d60bd5efc25dc 821a428e4eb2cf07 c7c7b2e375d4f92a 2048427fad7f7123 17076bac7d47bc0d 2e4fc8f44b1732e8 0f22fdb5d7a6cb13 66f63d15925141bb 1f2c5e00150095cc 2c5314f8e7b3bfa9 f7ea43692739852e  ....H/..c(.?...[Po.:._....F^..y`....5hY)Vm`.^.%...B.N.......u..* HB...q#..k.}G...O..K.2.."......f.=..QA..,^.....,S........Ci'9..
0000dc80: 89cbbda1482f0aeb 63280a3f02e7cc5b 506fca3a0c5feaa9 e3d4465ebed57960 a1cdc9e935685929 566d60bd5efc25dc 821a428e4eb2cf07 c7c7b2e375d4f92a 2048427fad7f7123 17076bac7d47bc0d 2e4fc8f44b1732e8 0f22fdb5d7a6cb13 66f63d15925141bb 1f2c5e00150095cc 2c5314f8e7b3bfa9 f7ea43692739852e  ....H/..c(.?...[Po.:._....F^..y`....5hY)Vm`.^.%...B.N.......u..* HB...q#..k.}G...O..K.2.."......f.=..QA..,^.....,S........Ci'9..
0000dd00: 89cbbda1482f0aeb 63280a3f02e7cc5b 506fca3a0c5feaa9 e3d4465ebed57960 a1cdc9e935685929 566d60bd5efc25dc 821a428e4eb2cf07 c7c7b2e375d4f92a 2048427fad7f7123 17076bac7d47bc0d 2e4fc8f44b1732e8 0f22fdb5d7a6cb13 66f63d15925141bb 1f2c5e00150095cc 2c5314f8e7b3bfa9 f7ea43692739852e  ....H/..c(.?...[Po.:._....F^..y`....5hY)Vm`.^.%...B.N.......u..* HB...q#..k.}G...O..K.2.."......f.=..QA..,^.....,S........Ci'9..
```

Could this be a bunch of ones that are being encrypted the same way?

I am going to keep chipping away at this a la Louis Agassiz. The longer I look at the file format, the more I notice. First line:

`xxd -g 8 -c 128 -e StroboClipHDC-1_0_15.sfu | head -n 1`

"SONUUSFW", 8009h (productId), 2368h (vendorId), 0xFC513B26 (no idea), "StroboClip HDC", zeroes for padding, 0x00016800, 15. (revision), 0 (minor version), 1 (major version), 1 (uh&hellip; the number one, it seems)

The gibberish _then_ starts for exactly 0x00016800 bytes. Where have I seen that number before?? (This, by the way, is 90 KB.)

Interestingly, the last 8 bytes do not repeat like all of the lines before. Either there is a strange way of ending the encryption or the end of the binary has some checksum or other information. (This is not unreasonable or odd.)

I wanted to find the exact spot where the repeats begin: `xxd -s 0xd000 -g 8 -c 128 -R always StroboClipHDC-1_0_15.sfu | head -n 24`

This is at 0xD8B8. Nice. This means the encryption is not happening in blocks and that the output is likely not used to seed encryption.

Taking a small tangent, LinkedIn shows Sonuus is a reasonably small company run by James Clark. There are patents! James Hastings Clark is the name you would find. Oh, and the one related to musical tuners has lapsed due to unpaid fees and will expire in 2028. That brings us to Peterson. Their two strobe tuner patents have expired. Someone else at BYU cited Peterson in US20220183925A1, which seems to vibrate your asscheeks to reduce your anxiety. What a world!

No word on what encryption his company uses. There is a small chance this is decoded before transmission by Peterson Connect. There is also a decent chance the debug port on the Cortex-M0+ CPU was left open. There is a very, very small possibility that some sort of firmware vulnerability would let you dump the flash contents via USB. The Sonuus utilities, if they can be accessed, might provide some insight. There are values we can use as an IV (including that mystery section from earlier).

On the Sonuus webpage, there are firmware files for various versions of different products. This lets us dive a little bit deeper into the file format. Looking at the early versions of i2M, Voluum, and Wahoo, we see that they all start with SONUUSFW and then a slightly different 16-bit value followed by static 16-bit value e.g. productId (800?h) and vendorId (231Ch). The next values are different but curiously always starting with F: 0xffa5702a, 0xfcb1fa0a, 0xf8083d99, 0xff5427f3, 0xff321cb7. I _really_ fail to see any pattern here, but so early in the binary would suggest either an encryption IV/key (although it's probably too short to be either) or a checksum. I guess technically all of these would be checksums of some sort.

Next is the product name padded with zeroes, then another field: 0xd800 for one file, 0x16000 for another, and 0x1a000 for the last, so I'll assume that 32-bit variable is the binary length. Next is clearly some four-digit version always ending with 1 as the last digit. Then gibberish. Gibberish that eventually repeats, but the repeated part is unique between products.

Again, between versions much of the tail-end gibberish is the same except the last 8 bytes. I haven't found any other correlation between products.

I have a sneaking suspicion that the product itself is supposed to contain the decryption algorithm (including product-unique key). Thus, unless the way of building one of these firmware files can be discovered, we might have to get much more creative in dumping the chip memory.

Now. Datasheet. The STM32L073xB has a pre-programmed bootloader and CRC calculation and no hardware decryption engine!

The next steps would be to check if the debug port is unlocked and to disassemble the Sonuus firmware uploader.

If the debug port is locked and the microcontroller is responsible for decrypting firmware, we might be stuck for a while until a more creative method of gaining access to the microcontroller is devised or the exact encryption technique is determined.

At the very least, there is a checksum of the entire file, which I suspect is that 32-bit thing, either a CRC32 or every long XOR'd together. If you change a single bit in the contents, the firmware file will not load into the update software.

This raises two questions:

1. What happens if a firmware file is created that has a valid checksum but invalid contents? (Will the board try to flash it anyway or is there a second data integrity check? If the board tries to flash, does this overwrite the bootloader or just the program? If just the program, can a valid firmware then be written?)
2. If the first 32 bits (or even the first 64 bits as well as what a valid vector table looks like) are KNOWN, is it still prohibitively hard to figure out the key?

<mark>TODO: deeper look into which patterns exist between firmware versions</mark>


## Checklist

- [x] udev rules from Peterson website
    - [x] get correct `idProduct` value(s): 0009h
- [ ] add photos of teardown
- [ ] sniff USB communication
- [ ] side quest: firmware
    - [x] locate firmware (It is encrypted.)
    - [ ] look at other firmware versions for patterns
    - [ ] capture firmware transfer
    - [x] check for encryption
    - [x] check for architecture
    - [ ] RTOS or bare metal?
    - [ ] see if startup time can be adjusted
    - [ ] see if a "normal" color can be selected
- [x] read serial number
- [ ] how are sweeteners formatted?
- [ ] mimic WebHID functionality
- [ ] CLI? GUI?
- [ ] continue editing this file
- [ ] check that SAST works

### Goals

According to the tuner documentation, the StroboClip software shall:

- [ ] update firmware
- [ ] design custom sweetened and guided tunings
- [ ] configure sweetener or guided tuning presets
- [ ] re-order, minimize, or expand on-board presets
- [ ] adjust various display settings

Available from the settings screen?

- [ ] display color
- [ ] accidentals preference (sharps, flats, both)
- [ ] user or default configuration
- [ ] auto-order presets
- [ ] factory request
- [ ] view firmware/hardware version (my device: 1010, 1006)

The duration for holding the power button in order to turn on is about half a second; off is about 1 second.

### Unrelated task

- [ ] APK for https://www.thomann.de/de/wittner_mt50_metronome.htm


## Getting started

To connect to the StroboClip, Linux needs to see and access the StroboClip by setting the correct device permissions. Then, any prerequisites for the client (such as a USB HID module for Python) should be installed. The client&mdash;whether CLI, GUI, or browser-based&mdash;could then be started. For browser-based access, any WebHID or WebUSB flags should be active. _Note: Firefox does not implement WebHID/WebUSB._

### Permissions

```sh
echo 'KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0664", GROUP="plugdev", ATTRS{idVendor}=="2368", ATTRS{idProduct}=="0009"' | sudo tee /etc/udev/rules.d/85-peterson-tuner.rules
echo 'KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0664", GROUP="plugdev", ATTRS{idVendor}=="2368", ATTRS{idProduct}=="8009"' | sudo tee -a /etc/udev/rules.d/85-peterson-tuner.rules
udevadm control --reload-rules && udevadm trigger
#sudo chmod a+rw /dev/hidraw*
sudo -k
```

### Prerequisites

```sh
pip install --user -r requirements.txt
```

### Client

```sh
python cli.py
```


## Developer Resources

<mark>TODO</mark>: list Wireshark dumps; mimic Shux repo structure/documentation

### Support and contributing

_Issue_ and/or _Merge request_


## Assorted complaints

- Preemptively, if you work at Peterson and are offended about third parties developing against your products, _**release your own Linux client**_. Granted, a browser HID solution is better than what 95 percent of electronics companies offer. If you release your own client, there would not be a strong reason to tinker around and discover your USB protocol.
- The GitLab _single file editor_ autocomplete is shitty. If you type `python cli.py` and press enter, it will autocomplete this line to the (incorrect) `python cli.python`. Also, the Gitlab editor adding a second quote/parenthesis/brace/etc. automatically is a buggy nuisance.
- To the guy/gal on a forum somewhere complaining about charging via USB and that it, &lsquo;is _so much easier_ to just replace a battery during a gig&rsquo;: What planet do you live on that it is more environmentally friendly **and** reliable to carry around at least one full CR2032 in a place you will always find it? You have to go to the store and buy more when they run out. Unless you tape it to the tuner, it can fall out of your case or wallet or pocket. Do you just throw the old one in the trash? Or stuff it in a pocket where it can discharge against all your other junk, i.e. keys, condoms, coins, glasses, tissues, hair ties? Put it in the holder for the replacement battery and try to remember to exchange it later? They make genuinely tiny, low-cost USB-C charging banks nowadays, and it is likely any smartphone from the past half decade can plug in for thirty to sixty seconds and load up enough power to tune a full set of new strings. Can you actually pop the battery out of its blister pack, take off the battery cover, remove the ol&mdash;<i>whoops! the new battery slipped onto the table... er, floor. shit.</i> Find the new battery on the floor, pick it up, remove the old battery (<i>&ldquo;Anyone have a pencil or corkscrew or something pointy?&rdquo;</i>), pack the old one away somewhere you won't forget to responsibly dispose of it, pop in the new battery, put the cover back on, remount the tuner&hellip; You can _really_ do that in less than a minute?? That feels more convenient than a 6-inch USB cable you carry around and plug into your phone and then the tuner, order a drink, come back and then tune? The **only** reasonable defense I have of a battery-powered tuner? Cost. You can get a working device for four dollars/euros/pounds, brand new, shipped across an entire ocean to your headstock, local economy be damned. The cheapest you can go right now for USB-charged is the Daddario PW-CT-24 for ten bucks. After three decent coin cell batteries, you get your money's worth on the rechargeable unit.


## License

MIT.
